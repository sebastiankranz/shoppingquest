@file:Suppress("UNCHECKED_CAST")

package danielschildnitz.shoppingquest.database

import arrow.core.Try
import com.google.firebase.firestore.DocumentSnapshot
import danielschildnitz.shoppingquest.model.ShoppingList
import danielschildnitz.shoppingquest.model.User
import danielschildnitz.shoppingquest.util.NonExistentDocumentException
import danielschildnitz.shoppingquest.util.asMap
import danielschildnitz.shoppingquest.util.asNumberToInt

fun DocumentSnapshot.toShoppingList(): ShoppingListOrError = Try {
    if (!exists()) throw NonExistentDocumentException()

    ShoppingList(
        id = id,
        name = getString("name"),
        color = null,
        openItemCount = getLong("openItemCount")?.toInt(),
        participants = get("participants")?.asMap<String, StringMap>()?.map {
            ShoppingList.Participant(
                user = User(
                    id = it.key,
                    name = it.value["name"] as? String,
                    picture = it.value["picture"]?.asNumberToInt()
                ),
                score = it.value["score"]?.asNumberToInt() ?: 0
            )
        } ?: listOf()
    )
}.toEither()
//
//fun StringMap.toParticipant() = ShoppingList.Participant(
//    user = User(
//        id = key,
//        name = value["name"] as? String,
//        picture = value["picture"]?.asNumberToInt()
//    ),
//    score = getvalue["score"]?.asNumberToInt() ?: 0
//)
