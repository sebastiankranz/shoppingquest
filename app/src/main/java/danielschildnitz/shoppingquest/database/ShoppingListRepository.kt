package danielschildnitz.shoppingquest.database

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import arrow.core.orNull
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import danielschildnitz.shoppingquest.model.Item
import danielschildnitz.shoppingquest.model.ShoppingList
import danielschildnitz.shoppingquest.model.User
import danielschildnitz.shoppingquest.util.CorruptedSnapshotException
import danielschildnitz.shoppingquest.util.SignedOutException
import danielschildnitz.shoppingquest.util.getOrThrow
import danielschildnitz.shoppingquest.util.rx.observeAuthState
import danielschildnitz.shoppingquest.util.rx.observeCollection
import danielschildnitz.shoppingquest.util.rx.observeDocument
import io.reactivex.Flowable
import io.reactivex.rxkotlin.combineLatest
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.tasks.await
import kotlin.math.max

typealias ShoppingListOrError = Either<Throwable, ShoppingList>
typealias ShoppingListListOrError = Either<Throwable, List<ShoppingListOrError>>
typealias UserOrError = Either<Throwable, User>
typealias ItemList = List<Either<Throwable, Item>>
typealias StringMap = Map<String, Any?>


class ShoppingListRepository(private val db: FirebaseFirestore, auth: FirebaseAuth) {

    init {
        db.firestoreSettings = FirebaseFirestoreSettings.Builder()
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .setPersistenceEnabled(true)
            .build()

    }


    val authState = auth.observeAuthState()


    fun getShoppingLists(): Flowable<List<ShoppingListOrError>> =
        db.collection("shoppinglists")
            .observeCollection()
            .map { list -> list.map { it.toShoppingList() } }


    fun getShoppingList(id: String): Flowable<ShoppingListOrError> =
        db.document("shoppinglists/$id")
            .observeDocument()
            .map { it.toShoppingList() }


    fun getItemsForShoppingList(listId: String): Flowable<ItemList> =
        db.collection("shoppinglists/$listId/items")
            .orderBy("name", Query.Direction.DESCENDING)
            .observeCollection()
            .map { list -> list.map { it.toItem() } }


    fun getShoppingListsForUser(userId: String): Flowable<List<ShoppingListOrError>> =
        db.collection("participation")
            .whereEqualTo("user", userId)
            .observeCollection()
            .switchMap { list: List<DocumentSnapshot> ->
                list
                    .map {
                        val listId = it.getString("list")
                            ?: return@map Flowable.just(
                                Left(CorruptedSnapshotException()) as ShoppingListOrError
                            )
                        getShoppingList(listId)
                    }
                    .combineLatest { it }
            }


    fun getUser(id: String) =
        db.document("users/$id")
            .observeDocument()
            .map { it.toUser(id) }


    fun getCurrentUser(): Flowable<UserOrError> =
        authState.switchMap {
            val userId = it.uid ?: return@switchMap Flowable.just(Left(SignedOutException()))
            getUser(userId).map { Right(it) }
        }


    suspend fun createShoppingList(name: String?) =
        db.collection("shoppinglists").add(
            mapOf("name" to name)
        ).await().id


    suspend fun joinShoppingList(userId: String, listId: String) {
        val userRef = db.document("users/$userId")
        val listRef = db.document("shoppinglists/$listId")

        db.collection("participation").add(mapOf("user" to userId, "list" to listId)).await()

        val user = userRef.get().await().toUser(userId)
        listRef.update("participants.$userId", user.asUpdateMap()).await()
    }


    fun updateScore(listId: String, userId: String, amount: Int) = db.runTransaction { t ->
        val shoppingListRef = db.document("shoppinglists/$listId")
        val oldScore =
            t.get(shoppingListRef).getDouble("participants.${userId}.score") ?: 0f.toDouble()
        t.update(
            shoppingListRef,
            "participants.${userId}.score",
            max(0f.toDouble(), oldScore + amount)
        )
    }


    fun updateItem(item: Item, listId: String, userId: String) {
        val itemRef = db.document("shoppinglists/$listId/items/${item.id}")
        db.runTransaction { transaction ->
            val old = transaction.get(itemRef).toItem().getOrThrow()
            when {
                !old.isChecked && item.isChecked -> {
                    updateScore(listId, userId, +1)
                    updateItemCount(listId, -1)
                }
                old.isChecked && !item.isChecked -> {
                    updateScore(listId, userId, -1)
                    updateItemCount(listId, +1)
                }
            }
            transaction.update(itemRef, item.asUpdateMap())
        }
    }

    fun updateItemCount(listId: String, amount: Int) = db.runTransaction { t ->
        val shoppingListRef = db.document("shoppinglists/$listId")
        val oldCount = t.get(shoppingListRef).toShoppingList().orNull()?.openItemCount ?: 0
        t.update(shoppingListRef, "openItemCount", max(0, oldCount + amount))
    }


    suspend fun removeItem(listId: String, itemId: String) {
        val itemRef = db.document("shoppinglists/$listId/items/$itemId")
        val item = itemRef.get().await().toItem().orNull()
        if (item?.isChecked == false) updateItemCount(listId, -1)
        itemRef.delete()
    }


    fun addItem(item: Item, listId: String) = GlobalScope.async {
        val itemRef = db.collection("shoppinglists/$listId/items")
            .add(item.asUpdateMap()).await()
        updateItemCount(listId, +1)
        return@async itemRef.id
    }


    suspend fun leaveShoppingList(userId: String, listId: String) {

        val participations = db.collection("participation")
            .whereEqualTo("user", userId)
            .whereEqualTo("list", listId)
            .get().await()

        if (participations.documents.isEmpty()) {
            throw Throwable("can't leave list, because it wasn't found")
        }

        participations.documents.forEach {
            val listId = it.getString("list")!!
            val listRef = db.document("shoppinglists/$listId")
            it.reference.delete().await()
            listRef.update("participants.$userId", FieldValue.delete()).await()
        }
    }


    suspend fun updateUser(userId: String, new: User) {
        val participations = db.collection("participation")
            .whereEqualTo("user", userId).get().await()
        participations.forEach {
            val listId = it.getString("list") ?: return@forEach
            new.asUpdateMap().forEach { (k, v) ->
                db.document("shoppinglists/$listId").update("participants.$userId.$k", v)
            }
        }
        db.document("users/$userId").set(new.asUpdateMap()).await()
    }


    fun changeListName(new: String, listId: String) {
        db.document("shoppinglists/$listId")
            .update("name", new)
    }

}