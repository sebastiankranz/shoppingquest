package danielschildnitz.shoppingquest.database

import arrow.core.Either
import arrow.core.Try
import com.google.firebase.firestore.DocumentSnapshot
import danielschildnitz.shoppingquest.model.Item
import danielschildnitz.shoppingquest.model.User
import danielschildnitz.shoppingquest.util.NonExistentDocumentException
import java.util.*

fun DocumentSnapshot.toItem(): Either<Throwable, Item> = Try {
    if (!exists()) throw NonExistentDocumentException()

    return@Try Item(
        id = id,
        name = getString("name"),
        amount = getString("amount"),
        isChecked = getBoolean("isChecked") ?: false,
        dateAdded = getDate("dateAdded") ?: Calendar.getInstance().time
    )
}.toEither()


fun Item.asUpdateMap(): Map<String, Any?> = mapOf(
    "name" to name,
    "amount" to amount,
    "isChecked" to isChecked
)


private fun getUserForId(userId: String, availableUsers: List<User>) =
    availableUsers.first() { it.id == userId }