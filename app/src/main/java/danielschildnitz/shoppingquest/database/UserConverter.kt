package danielschildnitz.shoppingquest.database

import com.google.firebase.firestore.DocumentSnapshot
import danielschildnitz.shoppingquest.model.User


fun DocumentSnapshot.toUser(authId: String) =
    User(
        id = authId,
        name = getString("name"),
        picture = getDouble("picture")?.toInt()
    )


fun User.asUpdateMap(): Map<String, Any?> = mapOf(
    "id" to id,
    "picture" to picture,
    "name" to name
)
