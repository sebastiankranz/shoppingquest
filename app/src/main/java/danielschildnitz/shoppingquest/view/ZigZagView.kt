package danielschildnitz.shoppingquest.view

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.ViewManager
import danielschildnitz.shoppingquest.R
import danielschildnitz.shoppingquest.util.ReactiveDelegate
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.dip


fun ViewManager.zigZagView(init: ZigZagView.() -> Unit = {}) =
    ankoView({ ZigZagView(it) }, theme = 0, init = init)

class ZigZagView : View {
    constructor(context: Context?) :
            super(context)

    constructor(context: Context?, attrs: AttributeSet?) :
            super(context, attrs)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) :
            super(context, attrs, defStyleAttr, defStyleRes)

    val r = ReactiveDelegate()

    val backdropColor = context.getColor(R.color.backdrop)
    val shadowColor = context.getColor(R.color.shadow)

    val backdropPaint = Paint().apply {
        color = backdropColor
    }

    val shadowPaint = Paint().apply {
        color = shadowColor
        maskFilter = BlurMaskFilter(15f, BlurMaskFilter.Blur.OUTER)
    }

    val strokePaint = Paint().apply {
        color = Color.parseColor("#bbbbbb")
        isAntiAlias = true
        strokeWidth = 2f
        strokeCap = Paint.Cap.SQUARE
        style = Paint.Style.STROKE
    }

    val zagWidth by r.manual(8)

    private val zagWidthPx by r.computed { dip(zagWidth) }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) = r.invalidateAll()

    val points by r.computed {
        val zigZagWidth = zagWidthPx * 2
        val zigZagAmount = width / zigZagWidth
        val zagAmount = zigZagAmount * 2
        val remainingPixels = width % zigZagWidth
        val remainingPerZag = remainingPixels.toFloat() / zagAmount
        (0..zagAmount).map {
            val x = (it * zagWidthPx) + (it * remainingPerZag)
            val y = if (it % 2 == 0) zagWidthPx else 0
            x to y + 1
        }
    }

    val shadowPath by r.computed {
        Path().apply {
            moveTo(0f, 0f)
            points.forEach { lineTo(it.first.toFloat(), it.second.toFloat()) }
            lineTo(width.toFloat(), 0f)
        }
    }

    val strokePath by r.computed {
        Path().apply {
            moveTo(0f, zagWidthPx.toFloat())
            points.forEach { lineTo(it.first, it.second.toFloat()) }
        }
    }

    val backdropPath by r.computed {
        Path().apply {
            moveTo(0f, height.toFloat())
            points.forEach { lineTo(it.first, it.second.toFloat()) }
            lineTo(width.toFloat(), height.toFloat())
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawPath(backdropPath, backdropPaint)
        canvas?.drawPath(shadowPath, shadowPaint)
        canvas?.drawPath(strokePath, strokePaint)
        shadowPath.reset()
    }

}