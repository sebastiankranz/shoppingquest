package danielschildnitz.shoppingquest.view

import android.os.Bundle
import android.view.*
import android.view.View.TEXT_ALIGNMENT_CENTER
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import arrow.core.Some
import arrow.core.orNull
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import danielschildnitz.shoppingquest.R
import danielschildnitz.shoppingquest.ShoppingQuestApplication
import danielschildnitz.shoppingquest.databinding.ViewJoinBinding
import danielschildnitz.shoppingquest.util.avatars
import danielschildnitz.shoppingquest.util.filterErrors
import danielschildnitz.shoppingquest.util.rx.observe
import danielschildnitz.shoppingquest.util.rx.textOnBlur
import danielschildnitz.shoppingquest.view.components.avatarView
import danielschildnitz.shoppingquest.view.components.hiddenEditText
import danielschildnitz.shoppingquest.view.components.outlinedButton
import danielschildnitz.shoppingquest.viewmodel.MainViewModel
import io.reactivex.Flowable
import io.reactivex.Observable
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.nestedScrollView
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject


class NavigationSheetFragment() : BottomSheetDialogFragment() {

    private lateinit var mViewModel: MainViewModel


    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory


    fun <T> Flowable<T>.observe(onNext: (T) -> Unit) =
        observe(viewLifecycleOwner, onNext)


    fun <T> Observable<T>.observe(onNext: (T) -> Unit) =
        observe(viewLifecycleOwner, onNext)


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity?.application as ShoppingQuestApplication).appComponent.inject(this)

        mViewModel = ViewModelProviders
            .of(activity!!, mViewModelFactory)
            .get(MainViewModel::class.java)

        return NavigationLayout().createView(AnkoContext.Companion.create(requireContext(), this))
    }


    // MARK: Listener Functions

    fun showAvatarSelectionDialog() {
        val avatarList = avatars.toList()
        val adapter = AvatarListAdapter(avatarList)
        MaterialAlertDialogBuilder(context)
            .setTitle(getString(R.string.title_avatar_seletction))
            .setAdapter(adapter, { _, pos ->
                mViewModel.changeAvatar(avatarList[pos].first)
            })
            .setNegativeButton(R.string.button_cancel, null)
            .show();
    }


    private fun showJoinDialog() {
        val binding = ViewJoinBinding.inflate(layoutInflater).apply {
            listIdField.requestFocus()
        }

        MaterialAlertDialogBuilder(context).setTitle(getString(R.string.title_invitation))
            .setMessage("Enter an invitation-id to join a list")
            .setView(binding.root)
            .setPositiveButton("Join", { _, _ -> joinList(binding.listId) })
            .setNegativeButton("Cancel", null)
            .show()
    }


    private fun joinList(id: String?) = GlobalScope.launch {
        try {
            if (id.isNullOrBlank()) throw IllegalArgumentException("You didn't specify an Id")
            mViewModel.joinShoppingList(id)
        } catch (e: Throwable) {
            MainScope().launch {
                toast("This list doesn't exist").show()
                e.printStackTrace()
            }
        }
    }


    private fun createList() = GlobalScope.launch {
//        try {
            val id = mViewModel.createShoppingList()
            mViewModel.joinShoppingList(id)
            mViewModel.selectedShoppingListId.onNext(Some(id))
//        } catch (e: Throwable) {
//            launch(Dispatchers.Main) { displayError(e) }
//        }

        dismiss()
    }


    fun displayError(e: Throwable) {
        Throwable(e).printStackTrace()
        toast(e.localizedMessage).show()
    }


    // MARK: Layout
    inner class NavigationLayout() : AnkoComponent<DialogFragment> {
        lateinit var nameEdit: EditText

        override fun createView(ui: AnkoContext<DialogFragment>): View = with(ui) {
            frameLayout {
                lparams(matchParent, matchParent)

                nestedScrollView {
                    lparams(matchParent, wrapContent)

                    frameLayout {
                        lparams(matchParent, wrapContent)

                        verticalLayout {
                            lparams(matchParent, wrapContent)

                            userView()
                                .lparams(matchParent, wrapContent)

                            shoppinglistList()
                                .lparams {
                                    width = matchParent
                                    bottomMargin = dip(8)
                                    topMargin = dip(16)
                                }

                            footerView()
                                .lparams { gravity = Gravity.CENTER }
                        }

                        sheetBackground()
                            .lparams {
                                width = matchParent
                                height = matchParent
                                topMargin = dip(42)
                            }

                    }
                }
            }
        }


        fun ViewManager.userView() = verticalLayout {
            gravity = Gravity.CENTER

            avatarView {
                mViewModel.userPicture.observe { imageResource = it.pictureResource }
                contentDescription = context.getString(R.string.your_image)
                setOnClickListener { showAvatarSelectionDialog() }
            }.lparams {
                height = dip(84)
                width = dip(84)
            }

            hiddenEditText {
                textSize = 20f
                hintResource = R.string.user_unnamed
                mViewModel.user.observe { setText(it.orNull()?.name ?: "") }
                textOnBlur().observe { mViewModel.changeUserName(it) }
                textAlignment = TEXT_ALIGNMENT_CENTER
            }.lparams {
                topMargin = dip(8)
            }
        }


        fun ViewManager.shoppinglistList() =
            recyclerView {
                val a = ShoppinglistListAdapter(onItemClick = {
                    mViewModel.selectedShoppingListId.onNext(Some(it.id))
                    dismiss()
                })
                adapter = a
                layoutManager = LinearLayoutManager(context)
                mViewModel.shoppingLists.observe {
                    a.shoppingLists = it.orNull()?.filterErrors() ?: listOf()
                }
            }


        fun ViewManager.footerView() = linearLayout {
            outlinedButton {
                textResource = R.string.create_list
                setOnClickListener { createList() }
            }.lparams {
                margin = dip(16)
                rightMargin = dip(8)
                height = dip(32)
            }

            outlinedButton {
                textResource = R.string.join_list
                setOnClickListener { showJoinDialog() }
            }.lparams {
                margin = dip(16)
                leftMargin = dip(8)
                height = dip(32)
            }
        }


        fun ViewManager.sheetBackground() = view {
            translationZ = -1f
            backgroundResource = R.drawable.background_bottom_sheet
        }
    }
}

