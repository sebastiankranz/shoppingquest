package danielschildnitz.shoppingquest.view.databinding

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("backgroundTint")
fun setBackgroundTint(
    view: View,
    tint: Int
) {
    view.background = view.background
        .mutate()
        .apply {
            setTint(tint)
            // setColorFilter( tint, PorterDuff.Mode.MULTIPLY )
        }


}