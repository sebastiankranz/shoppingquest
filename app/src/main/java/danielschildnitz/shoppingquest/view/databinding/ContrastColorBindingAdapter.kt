package danielschildnitz.shoppingquest.view.databinding

import android.graphics.Color
import android.util.Log
import android.widget.TextView
import androidx.databinding.BindingAdapter
import danielschildnitz.shoppingquest.util.calculateLuminance

@BindingAdapter("textColorContrastingTo")
fun setTextColor(
    view: TextView,
    color: Int
) {
    Log.v("lum", color.calculateLuminance().toString())
    val textColor = when {
        color.calculateLuminance() >= 0.5 -> Color.BLACK
        else -> Color.WHITE
    }
    view.setTextColor(textColor)
}


