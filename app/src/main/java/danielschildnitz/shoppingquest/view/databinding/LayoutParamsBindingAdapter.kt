package danielschildnitz.shoppingquest.view.databinding

import android.view.View
import android.widget.LinearLayout
import androidx.core.view.updateLayoutParams
import androidx.databinding.BindingAdapter


@BindingAdapter("layout_weight")
fun setLayoutWeight(view: View, weight: Float) = view.updateLayoutParams<LinearLayout.LayoutParams> {
    this.weight = weight
}