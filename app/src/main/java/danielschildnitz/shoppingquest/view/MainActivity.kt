package danielschildnitz.shoppingquest.view

import android.content.DialogInterface
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Gravity.BOTTOM
import android.view.Gravity.CENTER
import android.view.View
import android.view.View.NOT_FOCUSABLE
import android.view.ViewManager
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.DialogFragment.STYLE_NORMAL
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import arrow.core.Either
import arrow.core.None
import arrow.core.orNull
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth
import danielschildnitz.shoppingquest.R
import danielschildnitz.shoppingquest.ShoppingQuestApplication
import danielschildnitz.shoppingquest.databinding.ViewInvitationBinding
import danielschildnitz.shoppingquest.model.Item
import danielschildnitz.shoppingquest.util.filterErrors
import danielschildnitz.shoppingquest.util.rx.observe
import danielschildnitz.shoppingquest.util.rx.textOnBlur
import danielschildnitz.shoppingquest.util.setHidden
import danielschildnitz.shoppingquest.view.components.divider
import danielschildnitz.shoppingquest.view.components.hiddenEditText
import danielschildnitz.shoppingquest.view.components.subheader
import danielschildnitz.shoppingquest.viewmodel.MainViewModel
import io.reactivex.Flowable
import io.reactivex.Observable
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.navigationIconResource
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.nestedScrollView
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_SIGNIN = 1
    }

    private lateinit var mViewModel: MainViewModel


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory


    fun <T> Flowable<T>.observe(onNext: (T) -> Unit) =
        observe(this@MainActivity, onNext)

    fun <T> Observable<T>.observe(onNext: (T) -> Unit) =
        observe(this@MainActivity, onNext)


    // MARK: Entry Point
    @InternalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (application as ShoppingQuestApplication).appComponent.inject(this)

        mViewModel = ViewModelProviders
            .of(this, viewModelFactory)
            .get(MainViewModel::class.java)

        MainLayout().setContentView(this)

        val auth = FirebaseAuth.getInstance()
        if (auth.currentUser == null) {
            auth.signInAnonymously()
        }

        mViewModel.user.observe {
            if (it is Either.Left) Throwable(it.a).printStackTrace()
        }
    }


    // MARK: Action functions

    private fun showNavigationSheet() {
        val navigationFragment = NavigationSheetFragment().apply {
            setStyle(STYLE_NORMAL, R.style.ShareSheet)
        }
        navigationFragment.show(supportFragmentManager, navigationFragment.tag)
    }

    private fun leaveList() = GlobalScope.launch {
        //        try {
        mViewModel.leaveList()
        mViewModel.selectedShoppingListId.onNext(None)
//        } catch (e: Throwable) {
//            launch(Dispatchers.Main) { displayError(e) }
//        }
    }


    private fun showListId() {
        val binding = ViewInvitationBinding.inflate(layoutInflater).apply {
            listId = mViewModel.selectedShoppingListId.value?.orNull() ?: return
        }

        MaterialAlertDialogBuilder(this).setTitle(getString(R.string.title_invitation))
            .setMessage("Show this id to your friends, in order to allow them to join")
            .setView(binding.root)
            .setNegativeButton(
                "Done",
                DialogInterface.OnClickListener { _, _ -> return@OnClickListener })
            .show()
    }


    private fun displayError(e: Throwable) {
        Throwable(e).printStackTrace()
        toast(e.localizedMessage).show()
    }


    // MARK: Layout
    inner class MainLayout() : AnkoComponent<AppCompatActivity> {
        lateinit var nameEdit: EditText

        override fun createView(ui: AnkoContext<AppCompatActivity>): View = with(ui) {
            coordinatorLayout {
                lparams(matchParent, matchParent)

                frameLayout {

                    nestedScrollView {
                        lparams(matchParent, matchParent)
                        setHidden(true)
                        mViewModel.selectedShoppingList.observe { setHidden(it.isLeft()) }

                        shoppingListView().lparams(matchParent, matchParent)
                    }

                    shoppingListErrorView()
                }.lparams {
                    width = matchParent
                    height = wrapContent
                    bottomMargin = dip(56)
                }

                appBarContainer().lparams {
                    width = matchParent
                    height = wrapContent
                    gravity = BOTTOM
                }
            }
        }

        fun ViewManager.shoppingListView() =
            verticalLayout {

                nameEdit = hiddenEditText {
                    textSize = 24f
                    hintResource = R.string.list_unnamed
                    mViewModel.selectedShoppingList.observe { setText(it.orNull()?.name) }
                    textOnBlur().observe { mViewModel.changeListName(it) }
                }.lparams {
                    margin = dip(16)
                    topMargin = dip(0)
                    bottomMargin = 0
                }

                subheader {
                    mViewModel.memberCount.observe {
                        text = "${it.orNull()} Members:"
                    }
                }.lparams {
                    topMargin = dip(-24)
                    bottomMargin = dip(-16)
                }

                shoppingListContainer()

                divider()
                    .lparams {
                        height = dip(1)
                        width = matchParent
                        marginStart = dip(16)
                        marginEnd = dip(16)
                    }

                subheader("Groceries:")
                    .lparams {
                        bottomMargin = dip(-16)
                    }

                recyclerView {
                    val listener = object : ItemListAdapter.Callback {
                        override fun onItemRemove(id: String) {
                            mViewModel.removeItem(id)
                        }

                        override fun onItemChange(new: Item) {
                            mViewModel.updateItem(new)
                        }

                        override fun onItemAdd(new: Item): Deferred<String> =
                            mViewModel.addItem(new)
                    }
                    val a = ItemListAdapter(listener, lifecycleOwner = this@MainActivity)
                    adapter = a
                    layoutManager = LinearLayoutManager(context)
                    mViewModel.items.observe { itemsOrError ->
                        val items = itemsOrError.orNull() ?: listOf()
                        a.updateItems(items.filterErrors())
                    }
                }
            }


        fun ViewManager.shoppingListErrorView() =
            verticalLayout {
                setHidden(true)
                mViewModel.selectedShoppingList.observe { setHidden(it.isRight()) }
                gravity = CENTER

                imageView(R.drawable.ic_twotone_receipt_24px) {
                    alpha = 0.5f
                }.lparams(dip(56), dip(56))

                textView(R.string.no_list_selected) {
                    textSize = 14f
                }.lparams(wrapContent, wrapContent) {
                    topMargin = dip(16)
                }
            }


        fun ViewManager.appBarContainer() =
            constraintLayout {
                lparams(matchParent, wrapContent)

                ankoView(
                    { BottomAppBar(it) },
                    theme = R.style.TransparentBottomAppBar,
                    init = {
                        id = R.id.toolbar
                        navigationIconResource =
                            R.drawable.ic_menu_black_24dp
                        backgroundColor = Color.TRANSPARENT
                        replaceMenu(R.menu.main)
                        setOnMenuItemClickListener {
                            when (it.itemId) {
                                R.id.action_add_friend -> showListId()
                                R.id.action_leave_shoppinglist -> leaveList()
                                R.id.action_rename_shoppinglist -> nameEdit.requestFocus()
                                else -> return@setOnMenuItemClickListener false
                            }
                            return@setOnMenuItemClickListener true
                        }
                        setNavigationOnClickListener { showNavigationSheet() }
                    }
                ).lparams {
                    width = matchParent
                    topMargin = dip(4)
                    bottomToBottom = ConstraintSet.PARENT_ID
                    topToTop = ConstraintSet.PARENT_ID
                }

                divider()
                    .lparams {
                        height = dip(1)
                        margin = 0
                        bottomToBottom = R.id.toolbar
                    }

                zigZagView {
                    elevation = -1f
                }.lparams {
                    width = matchParent
                    height = 0
                    bottomToBottom = ConstraintSet.PARENT_ID
                    topToTop = ConstraintSet.PARENT_ID
                }
            }

        fun ViewManager.shoppingListContainer() = relativeLayout {
            lparams(matchParent, dip(168))

            recyclerView {
                rightPadding = dip(24)
                leftPadding = dip(24)
                clipToPadding = false
                isNestedScrollingEnabled = true
                val a = ScoreboardListAdapter()
                adapter = a
                layoutManager = LinearLayoutManager(
                    context, LinearLayout.HORIZONTAL, false
                )
                mViewModel.selectedShoppingList.observe { listOrError ->
                    a.participants = listOrError.orNull()?.participants
                        ?.sortedByDescending { it.score }
                        ?: listOf()
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    focusable = NOT_FOCUSABLE
                }
            }.lparams {
                height = dip(168)
            }

            progressBar {
                isIndeterminate = true
                mViewModel.selectedShoppingList.observe { listOrError ->
                    val participantCount = listOrError.orNull()?.participants?.size ?: 0
                    setHidden(participantCount > 0)
                }
            }.lparams {
                centerHorizontally()
                centerVertically()
            }
        }
    }


}
