package danielschildnitz.shoppingquest.view

import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import arrow.core.None
import arrow.core.Some
import danielschildnitz.shoppingquest.model.Item
import danielschildnitz.shoppingquest.util.orThrow
import danielschildnitz.shoppingquest.view.layout.itemCellLayout
import danielschildnitz.shoppingquest.viewmodel.ItemCellViewModel
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.withLatestFrom
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.AnkoContext
import kotlin.properties.Delegates


class ItemListAdapter(
    val callback: Callback,
    val lifecycleOwner: LifecycleOwner
) : RecyclerView.Adapter<ItemListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewModel = ItemCellViewModel()
        val view = itemCellLayout(
            AnkoContext.create(parent.context, parent),
            viewModel,
            lifecycleOwner
        )
        return ViewHolder(view, viewModel)
    }


//    override fun getItemId(position: Int): Long = cells[position].id.hashCode().toLong()


    fun onItemUpdate(new: Item, forPos: Int) {
        val cell = cells[forPos]
        if (cell.isItemInDatabase) callback.onItemChange(new)
        else GlobalScope.launch { val id = callback.onItemAdd(new).await() }
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int): Unit = with(holder) {
        viewModel.cell.onNext(Some(cells[position]))
        subscriptions.forEach { it.dispose() }
        subscriptions = listOf()
        subscriptions += viewModel.itemChangeEvents
            .subscribe { onItemUpdate(it.orNull() ?: throw IllegalStateException(), position) }
        subscriptions += viewModel.deleteEvents.withLatestFrom(viewModel.cell)
            .subscribe { (_, cell) -> callback.onItemRemove(cell.orThrow().item.id!!) }
    }


    override fun onViewRecycled(holder: ViewHolder) = with(holder) {
        holder.view.findFocus()?.clearFocus()
        viewModel.cell.onNext(None)
        subscriptions.forEach { it.dispose() }
        subscriptions = listOf()
    }


    private var cells: List<Cell> by Delegates.observable(listOf()) { _, old, new ->
//            DiffUtil.calculateDiff(DiffCallback(old, new)).dispatchUpdatesTo(this)
        notifyDataSetChanged() // <--- this fixed it.
    }


    fun updateItems(new: List<Item>) {
        cells = new
            .sortedByDescending { it.name }
            .map { Cell(it) }
            .plus(Cell()) // Add Item field
    }

    override fun getItemCount(): Int = cells.size


    inner class DiffCallback(
        private val oldList: List<Cell>,
        private val newList: List<Cell>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldList.size
        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldPos: Int, newPos: Int): Boolean =
            newList[newPos].id == oldList[oldPos].id
                    || newList[newPos].item.id == oldList[oldPos].item.id

        override fun areContentsTheSame(oldPos: Int, newPos: Int): Boolean =
            newList[newPos].item == oldList[oldPos].item

    }


    interface Callback {
        fun onItemChange(new: Item)
        fun onItemAdd(new: Item): Deferred<String>
        fun onItemRemove(id: String)
    }


    data class Cell(
        val item: Item = Item(),
        val id: String = item.id ?: Math.random().hashCode().toString()
    ) {
        val isItemInDatabase
            get() = item.id != null
    }


    class ViewHolder(
        val view: View,
        val viewModel: ItemCellViewModel
    ) : RecyclerView.ViewHolder(view) {
        var subscriptions: List<Disposable> = listOf()
    }


}