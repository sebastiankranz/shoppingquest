package danielschildnitz.shoppingquest.view.components

import android.view.ViewGroup
import android.view.ViewManager
import android.widget.Button
import androidx.annotation.StyleRes
import com.google.android.material.button.MaterialButton
import danielschildnitz.shoppingquest.R
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.ankoView

inline fun ViewManager.materialButton(
    @StyleRes theme: Int,
    init: MaterialButton.() -> Unit = {}
): MaterialButton =
    ankoView({ MaterialButton(it) }, theme, init = init)


fun ViewGroup.outlinedButton(init: Button.() -> Unit = {}) =
    button {
        textColorResource = R.color.colorPrimary
        textSize = 14f
        allCaps = true
        elevation = 0f
        maxHeight = dip(32)
        minWidth = dip(64)
        rightPadding = dip(16)
        leftPadding = dip(16)
        topPadding = 0;
        bottomPadding = 0;
        backgroundResource = R.drawable.background_outlined_button

        init()
    }
