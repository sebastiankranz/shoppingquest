package danielschildnitz.shoppingquest.view.components

import android.graphics.Color
import android.text.InputType
import android.view.View.TEXT_ALIGNMENT_GRAVITY
import android.view.ViewManager
import android.view.inputmethod.EditorInfo.IME_ACTION_DONE
import android.widget.EditText
import danielschildnitz.shoppingquest.R
import danielschildnitz.shoppingquest.util.rx.editorActions
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.editText
import org.jetbrains.anko.textColorResource

fun ViewManager.hiddenEditText(init: EditText.() -> Unit = {}) =
    editText {
        textColorResource = R.color.text_headline
        backgroundColor = Color.TRANSPARENT
        imeOptions = IME_ACTION_DONE
        maxLines = 1
        inputType = InputType.TYPE_CLASS_TEXT or
                InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
        textAlignment = TEXT_ALIGNMENT_GRAVITY

        editorActions().subscribe { clearFocus() }

        init()
    }