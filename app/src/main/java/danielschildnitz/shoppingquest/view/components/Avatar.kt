package danielschildnitz.shoppingquest.view.components

import android.view.ViewManager
import android.widget.ImageView
import danielschildnitz.shoppingquest.R
import org.jetbrains.anko.imageView

fun ViewManager.avatarView(init: ImageView.() -> Unit) =
    imageView {
        foreground = context.getDrawable(R.drawable.avatar_foreground)


        init()
    }

