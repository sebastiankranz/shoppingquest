package danielschildnitz.shoppingquest.view.components

import android.view.View
import android.view.ViewManager
import danielschildnitz.shoppingquest.R
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.view

fun ViewManager.divider(init: View.() -> Unit = {}) =
    view {
        backgroundColorResource = R.color.lightGrey
        init()
    }

fun ViewManager.dottedDivider(init: View.() -> Unit = {}) =
    view {
        backgroundResource = R.drawable.divider_cell
        init()
    }






