package danielschildnitz.shoppingquest.view.components

import android.view.ViewManager
import android.widget.TextView
import org.jetbrains.anko.dip
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.margin
import org.jetbrains.anko.textView

fun ViewManager.subheader(text: String = "", init: TextView.() -> Unit = {}) =
    frameLayout {
        textView(text) {
            // TODO: text color
            textSize = 14f
            init()
        }.lparams {
            margin = dip(16)
        }

    }
