package danielschildnitz.shoppingquest.view

import android.annotation.SuppressLint
import android.database.DataSetObserver
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import danielschildnitz.shoppingquest.R
import danielschildnitz.shoppingquest.databinding.CellAvatarBinding
import danielschildnitz.shoppingquest.model.User
import danielschildnitz.shoppingquest.util.Avatar


class AvatarListAdapter(val avatars: List<Pair<Int, Avatar>>) : BaseAdapter() {
    override fun getItem(position: Int): Any = avatars[position]
    override fun getItemId(position: Int): Long = avatars[position].first.toLong()
    override fun getCount(): Int = avatars.size


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val binding = convertView?.tag as? CellAvatarBinding ?: inflateBinding(parent ?: throw Exception())
        val avatar = avatars[position].second

        with(binding) {
            imageResource = avatar.pictureResource
            name = avatar.name
        }

        return binding.root
    }


    fun inflateBinding(parent: ViewGroup): CellAvatarBinding {
        val binding = DataBindingUtil.inflate<CellAvatarBinding>(
            LayoutInflater.from(parent.context),
            R.layout.cell_avatar,
            parent,
            false
        )
        binding.root.tag = binding
        return binding
    }

}