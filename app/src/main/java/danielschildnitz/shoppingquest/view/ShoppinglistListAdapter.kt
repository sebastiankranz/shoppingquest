package danielschildnitz.shoppingquest.view

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import danielschildnitz.shoppingquest.R
import danielschildnitz.shoppingquest.model.ShoppingList
import danielschildnitz.shoppingquest.util.getAvatarForImageResource
import danielschildnitz.shoppingquest.view.layout.ShoppingListCell
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import kotlin.properties.Delegates

class ShoppinglistListAdapter(
    val onItemClick: ((list: ShoppingList) -> Unit)?
) : RecyclerView.Adapter<ShoppinglistListAdapter.ShoppingListViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ShoppingListViewHolder {
        val view = ShoppingListCell().createView(
            AnkoContext.create(parent.context, parent)
        )

        return ShoppingListViewHolder(view)
    }


    override fun onBindViewHolder(holder: ShoppingListViewHolder, position: Int) {
        holder.showShoppingList(shoppingLists[position])
    }

    var shoppingLists: List<ShoppingList> by Delegates.observable(mutableListOf()) { _, old, new ->
        DiffUtil.calculateDiff(DiffCallback(old, new)).dispatchUpdatesTo(this)
    }


    override fun getItemCount(): Int = shoppingLists.size


    inner class DiffCallback(
        private val oldList: List<ShoppingList>,
        private val newList: List<ShoppingList>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldList.size
        override fun getNewListSize(): Int = newList.size
        override fun areItemsTheSame(oldPos: Int, newPos: Int): Boolean =
            oldList[oldPos].id == newList[newPos].id

        override fun areContentsTheSame(oldPos: Int, newPos: Int): Boolean =
            newList[newPos] == oldList[oldPos]
    }


    inner class ShoppingListViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun showShoppingList(new: ShoppingList?) =
            with(view) {
                find<TextView>(R.id.name).text = new?.name ?: ""
                find<TextView>(R.id.info).text =
                    "${new?.openItemCount ?: "0"} open items"
                ShoppingListCell.avatarViewIds.forEachIndexed { i, id ->
                    val participant = new?.participants?.getOrNull(i)
                    val avatarResourceId =
                        getAvatarForImageResource(
                            participant?.user?.picture
                        ).pictureResource
                    with(find<ImageView>(id)) {
                        setImageResource(avatarResourceId)
                        visibility = if (participant != null) View.VISIBLE else View.GONE
                    }
                }
                view.setOnClickListener {
                    if (new != null) onItemClick?.invoke(new)
                }
            }
    }
}