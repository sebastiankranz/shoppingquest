package danielschildnitz.shoppingquest.view.layout

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet.PARENT_ID
import danielschildnitz.shoppingquest.R
import danielschildnitz.shoppingquest.view.components.dottedDivider
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout


class ShoppingListCell() : AnkoComponent<ViewGroup> {

    companion object {
        val avatarViewIds = listOf<Int>(
            R.id.avatarView0,
            R.id.avatarView1,
            R.id.avatarView2,
            R.id.avatarView3,
            R.id.avatarView4
        )
    }

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        constraintLayout {
            lparams {
                width = matchParent
                minHeight = dip(64)
            }
            // backgroundResource = android.R.attr.selectableItemBackground


            dottedDivider()
                .lparams {
                    width = matchParent
                    height = dip(2)
                    marginStart = dip(16)
                    marginEnd = dip(16)
                    bottomToBottom = PARENT_ID
                }


            textView {
                id = R.id.name
                hintResource = R.string.list_unnamed
                textSize = 16f
                textColor = Color.parseColor("#DE000000")
            }.lparams {
                width = dip(0)
                marginStart = dip(16)
                topMargin = dip(8)
                marginEnd = dip(28)
                bottomMargin = dip(-8)
                startToStart = PARENT_ID
                topToTop = PARENT_ID
                endToStart = R.id.participantsList
            }


            textView {
                id = R.id.info
                textSize = 16f
                textColor = Color.parseColor("#99000000")
            }.lparams {
                width = 0
                verticalBias = 0f
                bottomMargin = dip(8)
                startToStart = R.id.name
                topToBottom = R.id.name
                bottomToBottom = PARENT_ID
            }


            linearLayout {
                id = R.id.participantsList

                avatarViewIds.reversed().forEachIndexed { i, viewId ->
                    imageView {
                        id = viewId
                        backgroundResource = R.drawable.avatar_background
                        padding = dip(2)
                    }.lparams {
                        width = dip(28)
                        height = dip(28)
                        marginEnd = if (i != avatarViewIds.lastIndex) dip(-8) else 0
                    }
                }

            }.lparams {
                height = matchParent
                marginEnd = dip(16)
                bottomToBottom = PARENT_ID
                endToEnd = PARENT_ID
                topToTop = PARENT_ID
            }
        }


    }
}




