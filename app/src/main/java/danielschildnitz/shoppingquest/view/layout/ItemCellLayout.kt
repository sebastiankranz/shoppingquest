package danielschildnitz.shoppingquest.view.layout

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.PopupMenu
import androidx.constraintlayout.widget.ConstraintSet
import androidx.lifecycle.LifecycleOwner
import arrow.core.Some
import danielschildnitz.shoppingquest.R
import danielschildnitz.shoppingquest.util.*
import danielschildnitz.shoppingquest.util.rx.*
import danielschildnitz.shoppingquest.view.components.dottedDivider
import danielschildnitz.shoppingquest.view.components.hiddenEditText
import danielschildnitz.shoppingquest.viewmodel.ItemCellViewModel
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.SingleSubject
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onAttachStateChangeListener
import java.util.concurrent.TimeUnit

fun itemCellLayout(
    ui: AnkoContext<ViewGroup>,
    viewModel: ItemCellViewModel,
    lifecycleOwner: LifecycleOwner
): View = with(ui) {

    fun <T> Observable<T>.observe(onNext: (T) -> Unit) = observe(lifecycleOwner, onNext)

    val nameField = SingleSubject.create<EditText>()
    val amountField = SingleSubject.create<EditText>()
    val editEvents = BehaviorSubject.create<Unit>()

    val isFocused = Observables.combineLatest(
        nameField.flatMapObservable { it.focusChanges() }.startWith(false),
        amountField.flatMapObservable { it.focusChanges() }.startWith(false)
    ) { name, amount -> name || amount }
        .distinctUntilChanged()
        .debounce(25, TimeUnit.MILLISECONDS)


    viewModel.cell.switchMap { c ->
        val trigger = if (c.orNull()?.isItemInDatabase == true) Observable.merge(
            amountField.flatMapObservable { it.confirmEvents() },
            nameField.flatMapObservable { it.blurEvents() },
            amountField.flatMapObservable { it.blurEvents() }
        ) else amountField.flatMapObservable { it.confirmEvents() }
        trigger.map { Unit }
    }.subscribe(viewModel.confirmEvents)


    return constraintLayout {
        lparams(matchParent, wrapContent)
        minHeight = dip(56)

        val isEditing = Observables.combineLatest(
            Observable.merge(editEvents.map { true }, isFocused.filter { !it }),
            viewModel.cell
        ) { triggerEvents, cell -> triggerEvents || cell.orNull()?.isItemInDatabase == false }
            .distinctUntilChanged()

        clicks().withLatestFrom(isEditing).filter { (_, iE) -> !iE }.map { Unit }
            .subscribe(viewModel.checkEvents)


        nameField.onSuccess(hiddenEditText {
            id = R.id.nameField
            textSize = 16f
            topPadding = dip(8)
            bottomPadding = dip(8)

            onAttachStateChangeListener { clearFocus() }
            viewModel.isChecked.observe {
                textColorResource = if (it) R.color.text_hint else R.color.text_secondary
            }
            isEditing.observe {
                hint = if (it) resources.getString(R.string.hint_item) else ""
                isEnabled = it
                requestFocus()
            }
            viewModel.confirmEvents.withLatestFrom(viewModel.name, viewModel.cell)
                .observe { (_, new, cell) ->
                    if (new.isBlank() && cell.orNull()?.isItemInDatabase == true) {
                    }
                    setText(cell.orNull()?.item?.name)
                }
            viewModel.cell.map { it.orNull()?.item?.name ?: "" }.observe { setText(it) }

            textChanges().subscribe(viewModel.name)

        }.lparams {
            marginStart = dip(20)
            bottomToBottom = androidx.constraintlayout.widget.ConstraintSet.PARENT_ID
            startToStart = androidx.constraintlayout.widget.ConstraintSet.PARENT_ID
            topToTop = androidx.constraintlayout.widget.ConstraintSet.PARENT_ID
        })


        amountField.onSuccess(hiddenEditText {
            textSize = 12f
            textColorResource = R.color.text_secondary
            topPadding = dip(8)
            bottomPadding = dip(8)

            onAttachStateChangeListener { clearFocus() }
            viewModel.isChecked.observe {
                textColorResource = if (it) R.color.text_hint else R.color.text_secondary
            }
            isEditing.observe {
                isEnabled = it
            }
            isFocused.observe {
                hint = if (it) resources.getString(R.string.hint_amount) else ""
            }
            nameField.flatMapObservable { it.confirmEvents() }
                .withLatestFrom(viewModel.cell)
                .observe { (_, cell) ->
                    if (cell.orNull()?.isItemInDatabase == false) requestFocus()
                }
            viewModel.cell.map { it.orNull()?.item?.amount ?: "" }.observe { setText(it) }

            textChanges().map { it }.subscribe(viewModel.amount)

        }.lparams {
            marginStart = dip(16)
            startToEnd = danielschildnitz.shoppingquest.R.id.nameField
            baselineToBaseline = danielschildnitz.shoppingquest.R.id.nameField
        })


        imageButton(R.drawable.ic_more) {
            id = R.id.optionsButton
            backgroundColor = Color.TRANSPARENT
            viewModel.cell.observe { setHidden(it.orNull()?.isItemInDatabase == false) }

            val menu = PopupMenu(ctx, this).apply {
                inflate(R.menu.item)
            }
            val menuItemClicks = menu.menuItemClicks()
            menuItemClicks
                .filter { it == R.id.action_edit_item }
                .map { Unit }
                .subscribe(editEvents)
            menuItemClicks
                .withLatestFrom(viewModel.cell)
                .filter { (aId, c) -> aId == R.id.action_remove_item && c is Some }
                .map { (_, c) -> c.orNull()!!.item.id }
                .subscribe(viewModel.deleteEvents)
            clicks().observe { menu.show() }
        }.lparams {
            height = dip(24)
            width = dip(24)
            endToEnd = ConstraintSet.PARENT_ID
            marginEnd = dip(16)
            topToTop = ConstraintSet.PARENT_ID
            bottomToBottom = ConstraintSet.PARENT_ID
        }


        dottedDivider()
            .lparams {
                width = matchParent
                height = dip(2)
                marginStart = dip(16)
                marginEnd = dip(16)
                bottomToBottom = androidx.constraintlayout.widget.ConstraintSet.PARENT_ID
            }


        view {
            backgroundColorResource = R.color.strikethrough
            setHidden(true)
            viewModel.isChecked.observe { setHidden(!it) }
        }.lparams {
            height = dip(1)
            width = 0
            marginEnd = dip(16)
            startToStart = R.id.nameField
            endToStart = R.id.optionsButton
            topToTop = ConstraintSet.PARENT_ID
            bottomToBottom = ConstraintSet.PARENT_ID
        }

    }

}