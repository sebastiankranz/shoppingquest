package danielschildnitz.shoppingquest.view

import androidx.recyclerview.widget.DiffUtil
import danielschildnitz.shoppingquest.databinding.CellScoreBinding
import danielschildnitz.shoppingquest.model.ShoppingList
import danielschildnitz.shoppingquest.R
import danielschildnitz.shoppingquest.model.maxScore
import danielschildnitz.shoppingquest.util.ReactiveClass
import danielschildnitz.shoppingquest.util.ReactiveDelegate
import danielschildnitz.shoppingquest.util.getAvatarForImageResource


class ScoreboardListAdapter() : DataBindingRecyclerViewAdapter(), ReactiveClass by ReactiveDelegate() {

    var participants: List<ShoppingList.Participant> by manual(listOf()) { old ->
        DiffUtil.calculateDiff(DiffCallback(old, participants)).dispatchUpdatesTo(this)
    }


    fun relativeScore(score: Int, maxScore: Int) = when (maxScore) {
        0 -> 0f
        else -> score.toFloat() / maxScore.toFloat()
    }



    override fun getLayoutIdForPosition(position: Int): Int = R.layout.cell_score


    override fun onBindDataBindingViewHolder(holder: ViewHolder, position: Int) =
        with(holder.binding as CellScoreBinding) {
            val participant = participants[position]
            val avatar = getAvatarForImageResource(participant.user?.picture)
            picture = avatar.pictureResource
            userColor = avatar.colorResource
            relativeScore = relativeScore(participant.score, participants.maxScore())
            absoluteScore = participant.score
        }



    override fun getItemCount(): Int = participants.size


    inner class DiffCallback(
        private val oldList: List<ShoppingList.Participant>,
        private val newList: List<ShoppingList.Participant>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldList.size
        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldPos: Int, newPos: Int): Boolean =
            newList[newPos].user?.id != null && newList[newPos].user?.id == oldList[oldPos].user?.id

        override fun areContentsTheSame(oldPos: Int, newPos: Int): Boolean {
            val oldRelativeScore = relativeScore(oldList.maxScore(), oldList[oldPos].score)
            val newRelativeScore = relativeScore(newList.maxScore(), newList[newPos].score)
            return newList[newPos] == oldList[oldPos]
                    && oldRelativeScore == newRelativeScore
        }

    }

/*    inner class Cell(val participant: ShoppingList.Participant) {
        fun relativeScore(maxScore: Int) = when (maxScore) {
            0 -> 0f
            else -> participant.score.toFloat() / maxScore.toFloat()
        }

        fun color = get

        fun textColor =
    }*/


}