package danielschildnitz.shoppingquest.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class DataBindingRecyclerViewAdapter() : RecyclerView.Adapter<DataBindingRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            viewType,
            parent,
            false
        )
        return ViewHolder(view)
    }


    open class ViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)


    override fun getItemViewType(position: Int) = getLayoutIdForPosition(position)


    abstract fun getLayoutIdForPosition(position: Int): Int


    abstract fun onBindDataBindingViewHolder(holder: ViewHolder, position: Int)


    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        onBindDataBindingViewHolder(holder, position)

}
