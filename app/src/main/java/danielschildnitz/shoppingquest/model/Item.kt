package danielschildnitz.shoppingquest.model

import java.util.*


data class Item(
    val id: String? = null, // if null -> not in DB yet
    val name: String? = null,
    val isChecked: Boolean = false,
    val dateAdded: Date = Calendar.getInstance().time,
    val amount: String? = null
) {
    fun isPrototype() = name.isNullOrBlank() // && !requestedBy.isEmpty()
}

