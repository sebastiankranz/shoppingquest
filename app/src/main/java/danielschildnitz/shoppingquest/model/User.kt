package danielschildnitz.shoppingquest.model

data class User(
    val id: String,
    val name: String? = null,
    val picture: Int? = DEFAULT_PICTURE
) {
    companion object {
        const val DEFAULT_PICTURE = 1
    }
}


