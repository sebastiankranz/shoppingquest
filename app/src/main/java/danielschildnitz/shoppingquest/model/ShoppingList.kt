package danielschildnitz.shoppingquest.model

data class ShoppingList(
    val id: String,
    val name: String?,
    val color: Int? = null,
    val participants: List<Participant> = listOf(),
    val openItemCount: Int?
) {

    data class Participant(
        val user: User?,
        val score: Int = 0
    )
}


fun List<ShoppingList.Participant>.getWinner() = maxBy { it.score }
fun List<ShoppingList.Participant>.maxScore() = getWinner()?.score ?: 0



