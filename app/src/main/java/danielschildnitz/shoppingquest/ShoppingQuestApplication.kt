package danielschildnitz.shoppingquest

import android.app.Application
import danielschildnitz.shoppingquest.dependencyinjection.AppComponent
import danielschildnitz.shoppingquest.dependencyinjection.AppModule
import danielschildnitz.shoppingquest.dependencyinjection.DaggerAppComponent


/**
 * Entry class for Dependency Injection
 */
class ShoppingQuestApplication: Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }


}