package danielschildnitz.shoppingquest.dependencyinjection

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import danielschildnitz.shoppingquest.ShoppingQuestApplication
import danielschildnitz.shoppingquest.database.ShoppingListRepository
import danielschildnitz.shoppingquest.viewmodel.CustomViewModelFactory

@Module
class AppModule(private val application: ShoppingQuestApplication) {

    @Provides
    fun provideCollectApplication(): ShoppingQuestApplication = application


    @Provides
    fun provideApplication(): Application = application


    @Provides
    fun provideViewModelFactory(repository: ShoppingListRepository): ViewModelProvider.Factory =
        CustomViewModelFactory(repository)


    @Provides
    fun provideFirestore(): FirebaseFirestore = FirebaseFirestore.getInstance()


    @Provides
    fun provideFirebaseAuth(): FirebaseAuth = FirebaseAuth.getInstance()


    @Provides
    fun provideRepository(database: FirebaseFirestore, auth: FirebaseAuth): ShoppingListRepository =
        ShoppingListRepository(database, auth)

}
