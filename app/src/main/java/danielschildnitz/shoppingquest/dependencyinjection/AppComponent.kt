package danielschildnitz.shoppingquest.dependencyinjection

import android.app.Application
import dagger.Component
import danielschildnitz.shoppingquest.view.MainActivity
import danielschildnitz.shoppingquest.view.NavigationSheetFragment
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
    fun inject(activity: NavigationSheetFragment)

    fun application(): Application
}