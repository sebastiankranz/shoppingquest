package danielschildnitz.shoppingquest.util

import android.graphics.Color
import android.view.View
import arrow.core.Either
import arrow.core.Option
import arrow.core.orNull
import java.util.*


@Suppress("UNCHECKED_CAST")
inline fun <reified X, reified Y> Any.asMap(): Map<X, Y>? {
    val m = this as Map<*, *>
    return when {
        isEmpty() -> m as Map<X, Y>
        m.keys.first() is X && m.values.first() is Y -> m as Map<X, Y>
        else -> null
    }
}


fun Any.asNumberToInt() = (this as? Number)?.toInt()


fun Int.calculateLuminance() = (
        0.299 * Color.red(this) +
                0.587 * Color.green(this) +
                0.114 * Color.blue(this)
        ) / 255f


fun <T> Either<Throwable, T>.getOrThrow(): T = fold({ throw Throwable(cause = it) }, { it })


fun View.setHidden(hidden: Boolean) {
    visibility = if (hidden) View.INVISIBLE else View.VISIBLE
}


fun <T : Any> List<Either<Any, T>>.filterErrors() = mapNotNull { it.orNull() }


fun <E> Iterable<E>.replace(index: Int, elem: E) =
    mapIndexed { i, existing -> if (i == index) elem else existing }


fun daysFromNow(days: Int) = Calendar.getInstance()
    .apply { add(Calendar.DATE, days) }
    .time


fun <T> Option<T>.orThrow() = orNull()!!