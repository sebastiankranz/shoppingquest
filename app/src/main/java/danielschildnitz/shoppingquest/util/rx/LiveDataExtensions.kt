package danielschildnitz.shoppingquest.util.rx

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations

fun <X, Y, Z> LiveData<X>.zipWith(other: LiveData<Y>, zipFunction: (X?, Y?) -> Z): LiveData<Z> {
    val data = MediatorLiveData<Z>()


    data.addSource(
        other,
        { data.value = zipFunction(this.value, it) }
    )

    data.addSource(
        this,
        { data.value = zipFunction(it, other.value) }
    )

    return data
}


fun <X, Y> LiveData<X>.map(mapFunction: (X) -> Y): LiveData<Y> =
    Transformations.map(this, mapFunction)


fun <X, Y> LiveData<X>.switchMap(mapFunction: (X) -> LiveData<Y>): LiveData<Y> =
    Transformations.switchMap(this, mapFunction)


fun <T> LiveData<T>.observe(owner: LifecycleOwner, onNext: (T) -> Unit) = observe(owner, Observer(onNext))

