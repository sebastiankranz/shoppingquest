package danielschildnitz.shoppingquest.util.rx

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.Observer
import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import org.reactivestreams.Publisher

fun <T> Publisher<T>.toLiveData(): LiveData<T> =
    LiveDataReactiveStreams.fromPublisher(this).map { it }

fun <T> Observable<T>.toFlowable(): Flowable<T> = toFlowable(BackpressureStrategy.LATEST)

fun <T> Publisher<T>.observe(owner: LifecycleOwner, onNext: (T) -> Unit) =
    toLiveData().observe(owner, Observer(onNext))

fun <T> Observable<T>.observe(owner: LifecycleOwner, onNext: (T) -> Unit) =
    toFlowable(BackpressureStrategy.LATEST).toLiveData().observe(owner, Observer(onNext))

fun <X, Y> Flowable<X>.switchMapCatching(mapper: (X) -> Flowable<Y>):
        Flowable<Either<Throwable, Y>> =
    this.switchMap { x ->
        try {
            mapper(x).map { Right(it) }
        } catch (e: Throwable) {
            Flowable.just(Left(e))
        }
    }


fun <X, Y> Flowable<X>.switchMapCatchingResult(mapper: (X) -> Flowable<Either<Throwable, Y>>):
        Flowable<Either<Throwable, Y>> =
    this.switchMap { x ->
        try {
            mapper(x).map { it }
        } catch (e: Throwable) {
            Flowable.just(Left(e))
        }
    }




