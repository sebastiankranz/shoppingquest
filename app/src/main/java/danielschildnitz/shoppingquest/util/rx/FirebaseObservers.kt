package danielschildnitz.shoppingquest.util.rx

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable

fun Query.observeCollection(
    strategy: BackpressureStrategy = BackpressureStrategy.DROP
): Flowable<List<DocumentSnapshot>> = Flowable.create({ emitter ->
    val listener = addSnapshotListener { snapshot, exception ->
        when {
            exception != null -> emitter.onError(exception)
            snapshot != null -> emitter.onNext(snapshot.documents)
        }
    }
    emitter.setCancellable { listener.remove() }
}, strategy)


fun DocumentReference.observeDocument(
    strategy: BackpressureStrategy = BackpressureStrategy.DROP
): Flowable<DocumentSnapshot> = Flowable.create({ emitter ->
    val listener = addSnapshotListener { snapshot, exception ->
        when {
            exception != null -> emitter.onError(exception)
            snapshot != null -> emitter.onNext(snapshot)
        }
    }
    emitter.setCancellable { listener.remove() }
}, strategy)


fun FirebaseAuth.observeAuthState(
    strategy: BackpressureStrategy = BackpressureStrategy.DROP
): Flowable<FirebaseAuth> = Flowable.create({ emitter ->
    val listener = FirebaseAuth.AuthStateListener { emitter.onNext(it) }
    addAuthStateListener(listener)
    emitter.setCancellable { removeAuthStateListener(listener) }
    emitter.onNext(this)
}, strategy)



