package danielschildnitz.shoppingquest.util.rx

import android.content.SharedPreferences
import arrow.core.toOption
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable

fun SharedPreferences.observeString(key: String) =
    observe(key, { it.getString(key, null).toOption() })


fun <T> SharedPreferences.observe(
    key: String,
    getValue: (pref: SharedPreferences) -> T
) = Flowable.create<T>(
    { emitter ->
        registerOnSharedPreferenceChangeListener { _, eventKey ->
            if (eventKey == key) emitter.onNext(
                getValue(this)
            )
        }
    },
    BackpressureStrategy.DROP
)

