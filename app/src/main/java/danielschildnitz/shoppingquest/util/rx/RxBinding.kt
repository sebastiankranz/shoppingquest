@file:Suppress("UNCHECKED_CAST")

package danielschildnitz.shoppingquest.util.rx

import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.PopupMenu
import danielschildnitz.shoppingquest.R
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.rxkotlin.withLatestFrom

fun <T> View.getCachedObservable(tagId: Int, create: (ObservableEmitter<T>) -> Unit): Observable<T> {
    val existingStream = getTag(tagId) as? Observable<T>
    if (existingStream != null)
        return existingStream

    val newStream = Observable.create(create).publish().autoConnect()
    setTag(tagId, newStream)
    return newStream
}


fun View.clicks() = getCachedObservable<Unit>(R.id.clicksStream) { emitter ->
    if (hasOnClickListeners()) throw IllegalStateException("onClickListener has already been set")
    setOnClickListener { emitter.onNext(Unit) }
    emitter.setCancellable { setOnClickListener(null) }
}


fun View.longClicks() = getCachedObservable<Unit>(R.id.clicksStream) { emitter ->
    setOnLongClickListener {
        emitter.onNext(Unit)
        true
    }
}


fun View.focusChanges() = getCachedObservable<Boolean>(R.id.focusChangesStream) { emitter ->
    if (onFocusChangeListener != null)
        throw IllegalStateException("onFocusChangeListener has already been set")
    setOnFocusChangeListener { _, hasFocus -> emitter.onNext(hasFocus) }
    emitter.setCancellable { onFocusChangeListener = null }
}


data class EditorActionEvent(val actionId: Int, val event: KeyEvent?)

fun EditText.editorActions() =
    getCachedObservable<EditorActionEvent>(R.id.editorActionStream) { emitter ->
        setOnEditorActionListener { _, actionId, event ->
            emitter.onNext(
                EditorActionEvent(
                    actionId,
                    event
                )
            )
            true
        }
        emitter.setCancellable { setOnEditorActionListener(null) }
    }


fun EditText.confirmEvents() = editorActions().filter {
    it.actionId == EditorInfo.IME_ACTION_DONE
}.map { Unit }


fun EditText.blurEvents() = focusChanges().filter { !it }.map { Unit }


fun EditText.textChanges() = getCachedObservable<String>(R.id.textChangesStream) { emitter ->
    val watcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            emitter.onNext(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    }
    addTextChangedListener(watcher)
    emitter.setCancellable { removeTextChangedListener(watcher) }
}


fun EditText.textOnBlur() = blurEvents()
    .withLatestFrom(textChanges()) { _, text -> text }


// Only use once
fun PopupMenu.menuItemClicks() = Observable.create<Int> { emitter ->
    setOnMenuItemClickListener {
        emitter.onNext(it.itemId)
        true
    }
    emitter.setCancellable { setOnDismissListener(null) }
}.publish().autoConnect()

