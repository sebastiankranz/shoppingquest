package danielschildnitz.shoppingquest.util

import danielschildnitz.shoppingquest.R

data class Avatar(val pictureResource: Int, val colorResource: Int, val name: String)

val avatars = mapOf(
    1 to Avatar(R.drawable.bat, R.color.bat, "Bat"),
    2 to Avatar(R.drawable.bear, R.color.bear, "Bear"),
    3 to Avatar(R.drawable.bee, R.color.bee, "Bee"),
    4 to Avatar(R.drawable.bird, R.color.bird, "Bird"),
    5 to Avatar(R.drawable.bug, R.color.bug, "Bug"),
    6 to Avatar(R.drawable.butterfly, R.color.butterfly, "Butterfly"),
    7 to Avatar(R.drawable.camel, R.color.camel, "Camel"),
    8 to Avatar(R.drawable.cat, R.color.cat, "Cat"),
    9 to Avatar(R.drawable.cheetah, R.color.cheetah, "Cheetah"),
    10 to Avatar(R.drawable.chicken, R.color.chicken, "Chicken"),
    11 to Avatar(R.drawable.coala, R.color.coala, "Coala"),
    12 to Avatar(R.drawable.cow, R.color.cow, "Cow"),
    13 to Avatar(R.drawable.crocodile, R.color.crocodile, "Crocodile"),
    14 to Avatar(R.drawable.dinosaur, R.color.dinosaur, "Dinosaur"),
    15 to Avatar(R.drawable.dog, R.color.dog, "Dog"),
    // 16 to Avatar(R.drawable.dolphin, R.color.dolphin, "Dolphin"),
    17 to Avatar(R.drawable.dove, R.color.dove, "Dove"),
    18 to Avatar(R.drawable.duck, R.color.duck, "Duck"),
    19 to Avatar(R.drawable.eagle, R.color.eagle, "Eagle"),
    20 to Avatar(R.drawable.elephant, R.color.elephant, "Elephant"),
    21 to Avatar(R.drawable.fish, R.color.fish, "Fish"),
    22 to Avatar(R.drawable.flamingo, R.color.flamingo, "Flamingo"),
    23 to Avatar(R.drawable.fox, R.color.fox, "Fox"),
    24 to Avatar(R.drawable.frog, R.color.frog, "Frog"),
    25 to Avatar(R.drawable.giraffe, R.color.giraffe, "Giraffe"),
    26 to Avatar(R.drawable.gorilla, R.color.gorilla, "Gorilla"),
    27 to Avatar(R.drawable.horse, R.color.horse, "Horse"),
    28 to Avatar(R.drawable.kangoroo, R.color.kangoroo, "Kangoroo"),
    29 to Avatar(R.drawable.leopard, R.color.leopard, "Leopard"),
    30 to Avatar(R.drawable.lion, R.color.lion, "Lion"),
    31 to Avatar(R.drawable.monkey, R.color.monkey, "Monkey"),
    32 to Avatar(R.drawable.mouse, R.color.mouse, "Mouse"),
    33 to Avatar(R.drawable.panda, R.color.panda, "Panda"),
    34 to Avatar(R.drawable.parrot, R.color.parrot, "Parrot"),
    35 to Avatar(R.drawable.penguin, R.color.penguin, "Penguin"),
    // 36 to Avatar(R.drawable.shark, R.color.shark, "Shark"),
    37 to Avatar(R.drawable.sheep, R.color.sheep, "Sheep"),
    38 to Avatar(R.drawable.snake, R.color.snake, "Snake"),
    39 to Avatar(R.drawable.spider, R.color.spider, "Spider"),
    40 to Avatar(R.drawable.squirrel, R.color.squirrel, "Squirrel"),
    41 to Avatar(R.drawable.starfish, R.color.starfish, "Starfish"),
    42 to Avatar(R.drawable.tiger, R.color.tiger, "Tiger"),
    43 to Avatar(R.drawable.turtle, R.color.turtle, "Turtle"),
    44 to Avatar(R.drawable.wolf, R.color.wolf, "Wolf"),
    45 to Avatar(R.drawable.zebra, R.color.zebra, "Zebra")
)

val DEFAULT_AVATAR = avatars[1]!!

fun getAvatarForImageResource(id: Int?): Avatar {
    return avatars[id] ?: DEFAULT_AVATAR
}
