package danielschildnitz.shoppingquest.util

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import arrow.core.none
import kotlin.reflect.KProperty


interface ReactiveClass {
    fun <T> manual(
        default: T,
        onChange: ((old: T) -> Unit)? = null
    ): ReactiveDelegate.ManualValDelegate<T>

    fun <T> computed(
        onChange: ((old: T) -> Unit)? = null,
        generate: () -> T
    ): ReactiveDelegate.ComputedValDelegate<T>
}


class ReactiveDelegate() : ReactiveClass {

    var computedDelegates: List<ComputedValDelegate<out Any?>> = listOf()



    override fun <T> manual(default: T, onChange: ((old: T) -> Unit)?): ManualValDelegate<T> =
        ManualValDelegate(default, onChange)


    override fun <T> computed(onChange: ((old: T) -> Unit)?, generate: () -> T): ComputedValDelegate<T> {
        val delegate = ComputedValDelegate(generate, onChange)
        computedDelegates += delegate
        return delegate
    }


    fun invalidateAll() {
        for (d in computedDelegates) {
            d.invalidate()
        }
    }


    inner class ManualValDelegate<T>(
        default: T,
        val onChange: ((old: T) -> Unit)?
    ) {
        var value = default

        operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
            return value
        }

        operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
            val oldValue = this.value
            this.value = value
            invalidateAll()
            onChange?.invoke(oldValue)
        }
    }


    inner class ComputedValDelegate<T>(
        val generate: () -> T,
        val onChange: ((old: T) -> Unit)?
    ) {

        var cachedValue: Option<T> = none()
        val value: T
            get() {
                if (cachedValue is None) cachedValue = Some(generate())
                return cachedValue.orNull()!!
            }

        operator fun getValue(thisRef: Any?, property: KProperty<*>): T = value

        fun invalidate() {
            val oldValue = value
            cachedValue = none()
            onChange?.invoke(oldValue)
        }
    }


}