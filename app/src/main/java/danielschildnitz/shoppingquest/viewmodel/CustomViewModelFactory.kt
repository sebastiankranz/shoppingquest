package danielschildnitz.shoppingquest.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import danielschildnitz.shoppingquest.database.ShoppingListRepository

@Suppress("UNCHECKED_CAST")
class CustomViewModelFactory(
    private val repository: ShoppingListRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            (modelClass.isAssignableFrom(MainViewModel::class.java)) ->
                MainViewModel(repository)

            else ->
                throw IllegalArgumentException("ViewModel not found")

        } as T
    }

}