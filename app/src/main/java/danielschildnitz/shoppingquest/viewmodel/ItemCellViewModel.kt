package danielschildnitz.shoppingquest.viewmodel

import android.util.Log
import arrow.core.Option
import arrow.core.Some
import danielschildnitz.shoppingquest.view.ItemListAdapter
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

class ItemCellViewModel {
    val cell = BehaviorSubject.create<Option<ItemListAdapter.Cell>>()

    val name = BehaviorSubject.create<String>()
    val amount = BehaviorSubject.create<String>()

    val confirmEvents = PublishSubject.create<Unit>()
    val checkEvents = PublishSubject.create<Unit>()
    val deleteEvents = PublishSubject.create<String>()

    val isChecked = cell.switchMap { c ->
        val start = c.orNull()?.item?.isChecked ?: false
        checkEvents.scan(start) { acc, _ -> !acc }
    }

    val newItem = Observables.combineLatest(name, amount, isChecked, cell)
    { name, amount, isChecked, cell ->
        cell.map { it.item.copy(name = name, amount = amount, isChecked = isChecked) }
    }

    val itemChangeEvents = Observable.merge(confirmEvents, checkEvents)
        .withLatestFrom(newItem) { _, it -> it }
        .filter { it is Some && !it.t.name.isNullOrBlank() }
        .distinctUntilChanged()

    init {
        deleteEvents.subscribe { Log.v("DeleteEvent", "asdf") }
        checkEvents.subscribe { Log.v("CheckEvents", "asdf")  }
    }
}