package danielschildnitz.shoppingquest.viewmodel

import androidx.lifecycle.ViewModel
import arrow.core.*
import danielschildnitz.shoppingquest.database.ShoppingListListOrError
import danielschildnitz.shoppingquest.database.ShoppingListOrError
import danielschildnitz.shoppingquest.database.ShoppingListRepository
import danielschildnitz.shoppingquest.model.Item
import danielschildnitz.shoppingquest.util.NothingSelected
import danielschildnitz.shoppingquest.util.getAvatarForImageResource
import danielschildnitz.shoppingquest.util.getOrThrow
import danielschildnitz.shoppingquest.util.orThrow
import danielschildnitz.shoppingquest.util.rx.switchMapCatching
import danielschildnitz.shoppingquest.util.rx.switchMapCatchingResult
import danielschildnitz.shoppingquest.util.rx.toFlowable
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.reactive.awaitFirst

class MainViewModel(private val repository: ShoppingListRepository) :
    ViewModel() {

    val selectedShoppingListId = BehaviorSubject.createDefault<Option<String>>(none())

    val user = repository.getCurrentUser()


    suspend fun getCurrentUser() = user.awaitFirst().getOrThrow()


    suspend fun getCurrentShoppingList() =
        selectedShoppingList.awaitFirst().getOrThrow()


    val userPicture = this.user
        .map { user -> getAvatarForImageResource(user.orNull()?.picture) }


    val selectedShoppingList: Flowable<ShoppingListOrError> =
        selectedShoppingListId.toFlowable().switchMapCatchingResult {
            if (it is Some) repository.getShoppingList(it.t) else throw NothingSelected()
        }


    val items =
        selectedShoppingListId.toFlowable().switchMapCatching { listId ->
            repository.getItemsForShoppingList(listId.getOrElse { throw NothingSelected() })
        }


    val shoppingLists: Flowable<ShoppingListListOrError> = user.switchMapCatching { userOrError ->
        repository.getShoppingListsForUser(userOrError.getOrThrow().id)
    }.cacheWithInitialCapacity(1)


    val memberCount = selectedShoppingList
        .map { s -> s.map { it.participants.size } }


    fun updateItem(item: Item) = GlobalScope.launch {
        repository.updateItem(item, selectedShoppingListId.value!!.orNull()!!, getCurrentUser().id)
    }


    fun addItem(item: Item) =
        repository.addItem(item, selectedShoppingListId.value!!.orNull()!!)


    suspend fun createShoppingList() = repository.createShoppingList("")


    suspend fun leaveList() =
        repository.leaveShoppingList(getCurrentUser().id, getCurrentShoppingList().id)


    fun changeAvatar(imageId: Int) = GlobalScope.launch {
        repository.updateUser(getCurrentUser().id, getCurrentUser().copy(picture = imageId))
    }

    suspend fun joinShoppingList(id: String) {
        repository.getShoppingList(id).awaitFirst().getOrThrow()

        repository.joinShoppingList(getCurrentUser().id, id)
    }


    fun changeListName(new: String) = GlobalScope.launch {
        repository.changeListName(new, getCurrentShoppingList().id)
    }


    fun changeUserName(new: String) = GlobalScope.launch {
        repository.updateUser(getCurrentUser().id, getCurrentUser().copy(name = new))
    }

    fun removeItem(id: String) = GlobalScope.launch {
        repository.removeItem(selectedShoppingListId.value!!.orThrow(), id)
    }
}